﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TaxCalculator.Models
{
    public class CalcualtorViewModel
    {
        public double NetAmount { get; set; }
        public double TaxRate { get; set; }
        public double GrossAmount { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    public class PostCaluclateTax
    {
        public double NetAmount { get; set; }
        public double TaxRate { get; set; }
    }
}
