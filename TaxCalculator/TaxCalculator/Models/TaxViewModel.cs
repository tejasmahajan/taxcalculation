﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaxCalculator.Models
{
    public class TaxViewModel
    {
        public string Description { get; set; }
        public int Value { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    public class PostTax
    {
        public string Description { get; set; }
        public int Value { get; set; }
    }
}
