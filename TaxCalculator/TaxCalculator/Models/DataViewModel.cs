﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaxCalculator.Models
{
    public class DataViewModel
    {
        public double NetAmount { get; set; }
        public double TaxRate { get; set; }
        public double GrossAmount { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
