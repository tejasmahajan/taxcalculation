import { Component, Inject, ChangeDetectionStrategy, AfterViewInit, ChangeDetectorRef, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from "rxjs";
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html'
})

export class CalculatorComponent {
  public taxes: Tax[];
  public calculationHistory: CalculationHistory[];
  public stateModel: any;
  public baseURL: string;
  public grossAmount: number;
  public model: any = {};


  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.baseURL = baseUrl;
    this.getTax();
    this.getCalculationHistory();
  }

  public getCalculationHistory() {
    this.http.get<CalculationHistory[]>(this.baseURL + 'api/Calculator/GetAll').subscribe(result => {
      this.calculationHistory = result;
    }, error => console.error(error));
  }

  public getTax() {
    this.http.get<Tax[]>(this.baseURL + 'api/Tax/GetAll').subscribe(result => {
      this.taxes = result;
    }, error => console.error(error));
  }

  public onSubmit() {
    this.postDataService(this.model).subscribe(data => {
      this.grossAmount = data;
      this.model.netAmount = '';
      this.model.taxRate = '';
      this.model.netAmount = '';
      this.grossAmount = '';
      this.getCalculationHistory();
    },
      error => {

        console.log(error);
      }
    );
  }

  public postDataService(calculatTax: CalculatTax): Observable<any> {
    var url = this.baseURL + 'api/Calculator/PostData';
    return this.http.post<any>(url, calculatTax, this.httpOptions)
      .pipe(
      map(res => {
          return res;
        })
      );
  }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

}

interface Tax {
  description: string;
  value: number;
}

interface CalculationHistory {
  netAmount: number;
  taxRate: number;
  grossAmount: number;
}

interface CalculatTax {
  netAmount: number;
  taxRate: number;
}
