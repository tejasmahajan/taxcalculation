import { Component, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from "rxjs";
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-tax',
  templateUrl: './tax.component.html'
})

export class TaxComponent {
  public taxes: Tax[];
  public searchTaxes: Tax[];
  public baseURL: string;
  public model: any = {};


  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.baseURL = baseUrl;
    this.getTax();
  }

  public getTax() {
    this.http.get<Tax[]>(this.baseURL + 'api/Tax/GetAll').subscribe(result => {
      this.taxes = result;
      this.searchTaxes = result;
    }, error => console.error(error));
  }

  public onSubmit() {
    this.postDataService(this.model).subscribe(data => {
      this.model.description = '';
      this.model.value = '';
      this.getTax();
    },
      error => {

        console.log(error);
      }
    );
  }

  public postDataService(addTax: Tax): Observable<any> {
    var url = this.baseURL + 'api/Tax/PostData';
    return this.http.post<any>(url, addTax, this.httpOptions)
      .pipe(
      map(res => {
          return res;
        })
      );
  }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };
  
  search(term: string) {
    if (!term) {
      this.taxes = this.searchTaxes;
    } else {
      this.taxes = this.searchTaxes.filter(x =>
        x.description.trim().toLowerCase().includes(term.trim().toLowerCase())
      );
    }
  }
}

interface Tax {
  description: string;
  value: number;
}

