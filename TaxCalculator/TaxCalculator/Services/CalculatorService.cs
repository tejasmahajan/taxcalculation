﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaxCalculator.Interface;
using TaxCalculator.Models;

namespace TaxCalculator.Services
{
    public class CalculatorService : ICalculator
    {
        public double CaluclateTax(List<CalcualtorViewModel> calcualtors, PostCaluclateTax caluclateTax)
        {
            try
            {
                var grossAmount = Math.Round(caluclateTax.NetAmount * ((1 + caluclateTax.TaxRate) / 100), 2);
                calcualtors.Add(new CalcualtorViewModel
                {
                    NetAmount = caluclateTax.NetAmount,
                    GrossAmount = grossAmount,
                    TaxRate = caluclateTax.TaxRate,
                    CreatedDate = DateTime.UtcNow
                });
                return grossAmount;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
