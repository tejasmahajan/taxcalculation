﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaxCalculator.Interface;
using TaxCalculator.Models;

namespace TaxCalculator.Services
{
    public class TaxService : ITax
    {
        public List<TaxViewModel> PostData(List<TaxViewModel> data, PostTax model)
        {
            data.Add(new TaxViewModel
            {
                Description = model.Description,
                Value = model.Value,
                CreatedDate = DateTime.UtcNow
            });
            return data;
        }
    }
}
