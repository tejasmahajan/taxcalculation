﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TaxCalculator.Interface;
using TaxCalculator.Models;

namespace TaxCalculator.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CalculatorController : ControllerBase
    {
        private readonly ICalculator _calculator;
        private static List<CalcualtorViewModel> calcualtorHistory;

        static CalculatorController()
        {
            calcualtorHistory = new List<CalcualtorViewModel>();

        }
        public CalculatorController(ICalculator calculator)
        {
            _calculator = calculator;
            //calcualtorHistory = new List<CalcualtorViewModel>() {
            //    new CalcualtorViewModel{ NetAmount=12,GrossAmount=123,TaxRate=12,CreatedDate=DateTime.UtcNow},
            //    new CalcualtorViewModel{ NetAmount=112,GrossAmount=12223,TaxRate=10,CreatedDate=DateTime.UtcNow},
            //    new CalcualtorViewModel{ NetAmount=1342,GrossAmount=12673,TaxRate=12,CreatedDate=DateTime.UtcNow}
            //};
        }

        [HttpGet("[action]")]
        public List<CalcualtorViewModel> GetAll()
        {
            return calcualtorHistory;
        }

        [HttpPost("[action]")]
        public IActionResult PostData(PostCaluclateTax postCaluclateTax)
        {
            var grossAmount = _calculator.CaluclateTax(calcualtorHistory, postCaluclateTax);
            return Ok(grossAmount);
        }
    }
}