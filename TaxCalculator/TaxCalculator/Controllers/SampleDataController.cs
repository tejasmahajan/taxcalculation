using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace TaxCalculator.Controllers
{
    [Route("api/[controller]")]
    public class SampleDataController : Controller
    {
        private static string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        public int solution(int[] A, int[] B, int N, int F)
        {
            // write your code in C99 (gcc 6.2.0)
            int loopIndex = F - 1;//0 -- F - 1
            int[] lower = new int[F];
            int[] upper = new int[F];
            //int upper[F];
            for (int i = 0; i < F; i++)
            {
                lower[i] = i;// indexs[i-1] < indexs[i] < indexs[i+1]
                upper[i] = N - F + i;
            }
            int sum = -1;
            int value1 = 1;
            while (value1==1)
            {
                //get current value
                int value = 0;
                int index = 0;
                for (int i = 0; i < N; i++)
                {
                    if (index < F && lower[index] == i)
                    {
                        value += A[i];
                        index++;
                    }
                    else
                    {
                        value += B[i];
                    }
                }
                if (sum < value)
                {
                    sum = value;
                }

                if (lower[loopIndex] < upper[loopIndex])
                {
                    //move the inner most index
                    lower[loopIndex] += 1;
                }
                else
                {
                    //adjust outer index
                    int outer = loopIndex - 1;
                    while (outer >= 0 && lower[outer] == upper[outer])
                    {
                        outer -= 1;
                    }
                    if (outer < 0)
                    {
                        break;
                    }
                    lower[outer] += 1;
                    outer++;
                    while (outer <= F - 1)
                    {
                        lower[outer] = lower[outer - 1] + 1;
                        outer++;
                    }
                }
            }
            return sum;
        }

        [HttpGet("[action]")]
        public IEnumerable<WeatherForecast> WeatherForecasts()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                DateFormatted = DateTime.Now.AddDays(index).ToString("d"),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            });
        }

        public class WeatherForecast
        {
            public string DateFormatted { get; set; }
            public int TemperatureC { get; set; }
            public string Summary { get; set; }

            public int TemperatureF
            {
                get
                {
                    return 32 + (int)(TemperatureC / 0.5556);
                }
            }
        }
    }
}
