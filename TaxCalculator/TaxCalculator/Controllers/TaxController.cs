﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TaxCalculator.Interface;
using TaxCalculator.Models;

namespace TaxCalculator.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaxController : ControllerBase
    {
        private readonly ITax _tax;
        private static List<TaxViewModel> taxes;

        static TaxController()
        {
            taxes = new List<TaxViewModel>() { new TaxViewModel { Description = "Standard Rate of Tax", Value = 20, CreatedDate = DateTime.UtcNow } };
        }
        public TaxController(ITax tax)
        {
            _tax = tax;
        }

        [HttpGet("[action]")]
        public List<TaxViewModel> GetAll()
        {
            return taxes;
        }

        [HttpPost("[action]")]
        public IActionResult PostData(PostTax postTax)
        {
            return Ok(_tax.PostData(taxes, postTax));
        }
    }
}