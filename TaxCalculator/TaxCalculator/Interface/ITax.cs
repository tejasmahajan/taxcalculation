﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaxCalculator.Models;

namespace TaxCalculator.Interface
{
    public interface ITax
    {
        List<TaxViewModel> PostData(List<TaxViewModel> data, PostTax model);
    }
}
